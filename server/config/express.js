/**
 * Created by kimduk on 27.05.14.
 */
var express = require('express'),
    bodyParser = require('body-parser');

module.exports = function (app, config) {
    app.set('port', process.env.PORT || config.port);
    app.set('views', config.rootPath + '/server/views');
    app.set('view engine', 'jade');
    app.use(express.static(config.rootPath + '/public'));
}