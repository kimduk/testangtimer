# angular-tasks — the test AngularJS apps
Test application with timetracker functional.
Using:
    angular - as main js framework
    angular-cookies to save local test data
    bootstrap as css framework

## Getting Started
Use cmd commands:
    npm install --save
    bower install --save