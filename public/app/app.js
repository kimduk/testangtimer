angular.module('app', ['ngResource', 'ngRoute', 'ngCookies']);

angular.module('app').config(function($routeProvider, $locationProvider) {
    $locationProvider.html5Mode(true);
    $routeProvider
        .when('/', { templateUrl: '/views/main/main.html', controller: 'mainCtrl'})

});

angular.module('app').run();
