angular.module('app').filter('momentFlt', function($filter) {
    return function(timestamp) {
        return moment.unix(timestamp).format('HH:mm:ss');
    };
});
