angular.module('app').filter('sumFlt', function($filter) {
    return function(rate, seconds) {
        return (seconds / 3600 * rate).toFixed(2);
    };
});
