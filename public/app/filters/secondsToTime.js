angular.module('app').filter('secondsToTime', function($filter) {
    return function(timeStart, timeFinish) {
        var seconds = (timeFinish !== undefined) ? timeFinish - timeStart : timeStart,
            hours = Math.floor(seconds / 3600),
            minutes = Math.floor(seconds / 60);

        minutes = (minutes >= 60) ? minutes - (60 * (hours)) : minutes;
        seconds = (seconds >= 60) ? seconds - (60 * (minutes)) - (3600 * (hours)) : seconds;

        hours = (hours < 10) ? '0' + hours : hours;
        minutes = (minutes < 10) ? '0' + minutes : minutes;
        seconds = (seconds < 10) ? '0' + seconds : seconds;

        return hours + ':' + minutes + ':' + seconds;
    };
});
