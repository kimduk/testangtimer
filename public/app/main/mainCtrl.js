angular.module('app')
    .controller('mainCtrl', function ($scope, $cookies, $interval, $filter, notifier) {
        var currentTimerValue = 0,
            currProcess,
            currTimerObj;
        $scope.currentTimerFormat = '00:00:00';
        $scope.timers = [];
        $scope.isRun = false;
        $scope.rate = 1;

        if ($cookies.get('timers') !== undefined) {
            $scope.timers = JSON.parse($cookies.get('timers'));
        }
        $scope.start = function() {
            if ( angular.isDefined(currProcess) ) return;

            var inputRate = document.getElementById("input-rate"),
                currentTime;

            $scope.rate = parseInt(inputRate.value);

            if (isNaN($scope.rate)) {
                notifier.error('Set Your Rate value!');
                return;
            }
            inputRate.disabled = true;
            currentTime = Math.floor(Date.now() / 1000);
            $scope.isRun = true;
            $scope.currentStartTime = moment.unix(currentTime).format('HH:mm:ss');

            currTimerObj = {
                name: $scope.issueName,
                rate: $scope.rate,
                timeStart: currentTime,
                timeFinish: 0
            };

            currProcess = $interval(function() {
                currentTimerValue++;
                $scope.currentTimerFormat = $filter('secondsToTime')(currentTimerValue);
            }, 1000);

            notifier.notify('Process is Start!');
        };
        $scope.stop = function() {
            var inputRate = document.getElementById("input-rate");
            if (angular.isDefined(currProcess)) {
                $interval.cancel(currProcess);
                currProcess = undefined;
                $scope.isRun = false;
                $scope.issueName = '';
                saveWork();
            }
            inputRate.disabled = false;
            notifier.notify('Process is Stop!');
        };

        $scope.$on('$destroy', function() {
            $scope.stop();
        });

        $scope.save = function() {
            $cookies.put('timers', JSON.stringify($scope.timers));
        };

        $scope.setEditItem = function(startTime) {
            $scope.editItem = getTimerByStart(startTime);
        };

        $scope.setDelItem = function(timeStart) {
            $scope.delItem = getTimerByStart(timeStart);
        };
        /**
         * Delete Element from Timer by 'startTime'
         * @param timeStart
         */
        $scope.delete = function(timeStart)  {
            var result = [];
            $scope.timers.forEach(function(el) {
                if(el.timeStart !== timeStart) {
                    result.push(el);
                }
            });
            $scope.timers = result;
            $cookies.put('timers', JSON.stringify($scope.timers));

            notifier.notify('Delete is Complete!');
        };
        /**
         * Get Timer item by 'timeStart'
         * @param timeStart
         * @returns {{}}
         */
        var getTimerByStart = function(timeStart) {
            var item = {};
            $scope.timers.forEach(function(el) {
                if(el.timeStart === timeStart) {
                    item = el;
                }
            });
            return item;
        }
        /**
         * Save current Timer object to $scope and cookies
         */
        var saveWork = function() {
            currTimerObj.timeFinish = Math.floor(Date.now() / 1000);
            $scope.timers.push(currTimerObj);
            $cookies.put('timers', JSON.stringify($scope.timers));
        };
    });
